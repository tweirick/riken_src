shell.prefix("source ~/.bash_profile ;")

KALLISTO_INDEX = config["kallisto_index"]
ANNOTATIONS    = config["annotations"]
REF_GENOME     = config["reference_genome"]
FASTQ_SUFFIX   = config["fastq_suffix"] # fastq
READ_STYLE     = config["read_style"] # _R
IS_SINGLE      = config["is_single"]

if IS_SINGLE.upper() == "TRUE":
    IS_SINGLE = True
elif IS_SINGLE.upper() == "FALSE":
    IS_SINGLE = False
else:
    assert False, "ERROR: is_single must be true or false."

SRA_DIR = "SRA/"
SRAS = SRA_DIR + "{sample}.sra"

FASTQ_DIR = "FASTQS/"
SINGLE_FASTQ         = FASTQ_DIR + "{sample}.%s" % FASTQ_SUFFIX
SINGLE_TRIMMED_FASTQ = FASTQ_DIR + "{sample}.trimmed.%s" % FASTQ_SUFFIX

PAIRED_FASTQ_1                  = FASTQ_DIR + "{sample}%s1.%s" % (READ_STYLE, FASTQ_SUFFIX)
PAIRED_FASTQ_2                  = FASTQ_DIR + "{sample}%s2.%s" % (READ_STYLE, FASTQ_SUFFIX)
PAIRED_TRIMMED_FASTQ_1          = FASTQ_DIR + "{sample}_1.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_FASTQ_2          = FASTQ_DIR + "{sample}_2.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_1 = FASTQ_DIR + "{sample}_unpaired_1.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_2 = FASTQ_DIR + "{sample}_unpaired_2.%s" % FASTQ_SUFFIX
TRIMMED_FASTQ                   = FASTQ_DIR + "{sample}_{ext}.trimmed.%s" % FASTQ_SUFFIX

LOG_DIR = "LOG/"
QC_HTML = LOG_DIR + "{sample}.trim.html"
QC_JSON = LOG_DIR + "{sample}.trim.json"

KALLISTO_DIR = "KALLISTO/"
ABUNDANCE_H5   = KALLISTO_DIR + "{sample}/abundance.h5"
ABUNDANCE_TSV  = KALLISTO_DIR + "{sample}/abundance.tsv"
ABUNDANCE_JSON = KALLISTO_DIR + "{sample}/run_info.json"
PSEUDOALIGNMENTS_BAM = KALLISTO_DIR + "{sample}/pseudoalignments.bam"

GENE_COUNT_DIR = "GENE_COUNTS/"
GENE_COUNTS    = GENE_COUNT_DIR + "{sample}.gene_counts.tsv"

CUFFDIFF_DIR = "CUFFDIFF/"


SAMPLES = []
group_dict = {}
for line in open(config['samples']):
    sl = line.strip().split()
    sample_id = sl[0]
    group_name = sl[-1]
    SAMPLES.append(sample_id)
    try:
        group_dict[group_name].append(sample_id)    
    except KeyError: 
        group_dict[group_name] = [sample_id]

cuffdiff_samples_list = []
for group_name in sorted(group_dict): 
    cuffdiff_samples_list.append( 
        ",".join(
            expand(PSEUDOALIGNMENTS_BAM, sample=sorted(group_dict[group_name]))
        )
    ) 

cuffdiff_groups  = ",".join(sorted(group_dict))
cuffdiff_samples = " ".join(cuffdiff_samples_list)

CUFFDIFF_DIR = "CUFFDIFF/" + "__vs__".join( sorted(group_dict)) + "/"
CUFFDIFF_FILE = CUFFDIFF_DIR + "gene_exp.diff"

rule all: 
    input: 
        expand(GENE_COUNTS, sample=SAMPLES),
        CUFFDIFF_FILE

if IS_SINGLE:
     
    rule fastqdump__unzip_fastq_from_sra:
        input: SRAS
        output: temp(SINGLE_FASTQ),
        params: fastq_dir = FASTQ_DIR
        shell:
            """ fastq-dump --split-3 --O {params.fastq_dir} {input} ; """
            """ touch -c {output}  ; """
    
    rule single_trim_fastqs:
        input: SINGLE_FASTQ
        output:
            trimmed_fastq = temp(SINGLE_TRIMMED_FASTQ),
            qc_html       = QC_HTML,
            qc_json       = QC_JSON
        priority: 2
        threads: 8
        shell:
            """ fastp --thread {threads} --trim_front1 5 --cut_by_quality5 --cut_by_quality3 --overrepresentation_analysis """
            """ --html {output.qc_html} --json {output.qc_json}  --in1 {input} --out1 {output.trimmed_fastq} """
     
    rule kallisto__align_reads:
        input: SINGLE_TRIMMED_FASTQ
        params:
            index = KALLISTO_INDEX,
            out_dir = KALLISTO_DIR,
            annotations      = config["annotations"],
            chromosome_sizes = config["chromosome_sizes"]
        output:
            abundance_h5  = ABUNDANCE_H5,
            abundance_tsv = ABUNDANCE_TSV,
            run_info_json = ABUNDANCE_JSON,
            pseudo_bam    = PSEUDOALIGNMENTS_BAM
        shell:
            """ kallisto quant --single --genomebam --threads 4 --bootstrap-samples 10      """
            """ -l 55 """
            """ -s 530 """
            """     --gtf {params.annotations} --chromosome {params.chromosome_sizes} """
            """     --index {params.index}  -o {params.out_dir}{wildcards.sample} """
            """     {input}  ; """
            """ touch -c {output.pseudo_bam} {output.abundance_h5} {output.abundance_tsv} {output.run_info_json}"""
    
else:
    
    rule fastqdump__unzip_fastq_from_sra:
        input: SRAS
        output:
            fastq_1 = temp(PAIRED_FASTQ_1),
            fastq_2 = temp(PAIRED_FASTQ_2)
        params: fastq_dir = FASTQ_DIR
        shell: 
            """ fastq-dump --split-3 --O {params.fastq_dir} {input} ; """ 
            """ touch -c {output.fastq_1} {output.fastq_2} ; """
    
    rule fastp__trim_fastqs:
        input:
            fastq_1 = PAIRED_FASTQ_1,
            fastq_2 = PAIRED_FASTQ_2
        output:
            trimmed_pd_fq_1 = temp(PAIRED_TRIMMED_FASTQ_1),
            trimmed_pd_fq_2 = temp(PAIRED_TRIMMED_FASTQ_2),
            qc_html = QC_HTML,
            qc_json = QC_JSON,
        priority: 2
        threads: 8
        shell:
            """fastp """
            """ --cut_by_quality5 """             # enable per read cutting by quality in front (5'), default is disabled (WARNING: this will interfere deduplication for both PE/SE data)
            """ --cut_by_quality3 """             # enable per read cutting by quality in tail (3'), default is disabled (WARNING: this will interfere deduplication for SE data)
            """ --detect_adapter_for_pe """       # by default, the auto-detection for adapter is for SE data input only, turn on this option to enable it for PE data.
            """ --overrepresentation_analysis """ # enable overrepresented sequence analysis.
            """ --correction """                  # enable base correction in overlapped regions (only for PE data), default is disabled
            """ --trim_front1 7 --trim_front2 7 """
            """ --in1  {input.fastq_1} --in2 {input.fastq_2} --json {output.qc_json} --html {output.qc_html} --out1 {output.trimmed_pd_fq_1} --out2 {output.trimmed_pd_fq_2} """
    
    rule kallisto__align_reads: 
        input: 
            fastq_1 = PAIRED_TRIMMED_FASTQ_1,
            fastq_2 = PAIRED_TRIMMED_FASTQ_2
        params: 
            index = KALLISTO_INDEX,
            out_dir = KALLISTO_DIR,
            annotations      = config["annotations"],
            chromosome_sizes = config["chromosome_sizes"]
        output: 
            abundance_h5  = ABUNDANCE_H5, 
            abundance_tsv = ABUNDANCE_TSV, 
            run_info_json = ABUNDANCE_JSON,
            pseudo_bam = PSEUDOALIGNMENTS_BAM
        shell: 
            """ kallisto quant --genomebam --threads 4 --bootstrap-samples 10      """
            """  --gtf {params.annotations} --chromosome {params.chromosome_sizes} """
            """     --index {params.index}  -o {params.out_dir}{wildcards.sample} {input.fastq_1} {input.fastq_2} ; """
            """ touch -c {output.pseudo_bam} {output.abundance_h5} {output.abundance_tsv} {output.run_info_json}""" 
# END IF 
 
rule cuffdiff__find_differentially_expressed_genes:
    input: expand(PSEUDOALIGNMENTS_BAM, sample=SAMPLES)
    params: 
        ref_genome = REF_GENOME,
        gtf        = ANNOTATIONS,
        out_dir    = CUFFDIFF_DIR,
        labels     = cuffdiff_groups, 
        groups     = cuffdiff_samples,
    output: CUFFDIFF_FILE
    priority: 50
    shell: 
        """ cuffdiff  --num-threads 10 --output-dir {params.out_dir} """
        """     --labels {params.labels} """
        """     --frag-bias-correct {params.ref_genome} """ 
        """    {params.gtf} {params.groups} ; """
        """ cp {output} {output}.bak ; """
        """ touch -c {input}         ; """
        """ touch -c {output}        ; """

rule htseqcount__get_gene_counts:
    input: PSEUDOALIGNMENTS_BAM
    params: annotations = ANNOTATIONS 
    output: GENE_COUNTS
    shell: 
        """ htseq-count {input} {params.annotations} --idattr gene_id --type gene --stranded=no --format bam > {output} """

