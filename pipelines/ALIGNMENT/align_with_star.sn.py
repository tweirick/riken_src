shell.prefix("source ~/.bash_profile ;")
import os
WORKING_DIR = os.getcwd() + "/"

#ANNOTATIONS    = config["annotations"]
FASTQ_SUFFIX   = config["fastq_suffix"] # fastq
READ_STYLE     = config["read_style"] # _R
IS_SINGLE      = config["is_single"]
IS_GZIPPED     = config["gzip"]
assert type(IS_SINGLE) == bool, "must be bool"

MIN_READ_LEN = config["min_read_len"]

fastp_txt = ""
star_txt = ""

gz_command   = "--readFilesCommand zcat" if IS_GZIPPED else ""
fastq_txt    = " --compression "         if IS_GZIPPED else ""
FASTQ_SUFFIX = FASTQ_SUFFIX + ".gz"      if IS_GZIPPED else ""
DUMP_GZIP    = " --gzip " if IS_GZIPPED else ""

sample_name = config['samples'].split("/")[-1].split(".")[0] + "."

SRA_DIR = "SRA/"
# Note: fastq-dump will move into the SRA directory 
# toaccount for the differences in behavior when using dbGaP vs SRA data.
SRA_FILE_NAME = "{sample}.sra"
SRAS = SRA_DIR + "{sample}.sra"

# Note: Using WORKING DIR here to account for the differences in behavior of 
# fastq-dump when using SRA vs dbGaP data. 
FASTQ_DIR = WORKING_DIR + "FASTQS/"
SINGLE_FASTQ         = FASTQ_DIR + "{sample}.%s" % FASTQ_SUFFIX
SINGLE_TRIMMED_FASTQ = FASTQ_DIR + "{sample}.trimmed.%s" % FASTQ_SUFFIX

PAIRED_FASTQ_1                  = FASTQ_DIR + "{sample}%s1.%s" % (READ_STYLE, FASTQ_SUFFIX)
PAIRED_FASTQ_2                  = FASTQ_DIR + "{sample}%s2.%s" % (READ_STYLE, FASTQ_SUFFIX)
PAIRED_TRIMMED_FASTQ_1          = FASTQ_DIR + "{sample}_1.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_FASTQ_2          = FASTQ_DIR + "{sample}_2.trimmed.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_1 = FASTQ_DIR + "{sample}_unpaired_1.%s" % FASTQ_SUFFIX
PAIRED_TRIMMED_UNPAIRED_FASTQ_2 = FASTQ_DIR + "{sample}_unpaired_2.%s" % FASTQ_SUFFIX
TRIMMED_FASTQ                   = FASTQ_DIR + "{sample}_{ext}.trimmed.%s" % FASTQ_SUFFIX

FIG_FORMAT = "png"
LOG_DIR = "LOG/"
QC_HTML = LOG_DIR + "{sample}.trim.html"
QC_JSON = LOG_DIR + "{sample}.trim.json"
STAR_JOINED           = LOG_DIR + sample_name + "joined_star_alignment_logs.tsv"
STAR_JOINED_FORMATTED = LOG_DIR + sample_name + "star_alignment_logs.tsv" 

ALIGNMENT_FIG = LOG_DIR + sample_name + "alignments_overview." + FIG_FORMAT

GENE_COUNT_DIR = "GENE_COUNTS/"
GENE_COUNTS    = GENE_COUNT_DIR + "{sample}.gene_counts.tsv"

CUFFDIFF_DIR = "CUFFDIFF/"

RESULTS_DIR       = "RESULTS/"
# For building count matrix.
GENE_COUNT_MATRIX = RESULTS_DIR + sample_name +  "gene_counts_star.matrix.tsv"

# Output of edgeR
SIZE_FACTOR = RESULTS_DIR + sample_name + "size_factors.txt"
BCV_FILE    = RESULTS_DIR + sample_name + "BCV.pdf"
GLMQLF      = RESULTS_DIR + sample_name + "glmqlf_diffexp.tsv"
CPM_VALS    = RESULTS_DIR + sample_name + "CPM_VALS.tsv"
DEG_COUNTS  = RESULTS_DIR + sample_name + "DEG_counts.tsv"
MA_PLOT     = RESULTS_DIR + sample_name + "MA_plot.pdf"
MDS_PLOT    = RESULTS_DIR + sample_name + "MDS_plot.pdf"

#DIFF_EXP_GENES    = RESULTS_DIR + "diff_exp_genes.tsv"
#MA_PLOT           = RESULTS_DIR + "ma_plot.pdf"

SAMPLES = []
group_dict = {}
for line in open(config['samples']):
    sl = line.strip().split()
    sample_id = sl[0]
    group_name = sl[-1]
    if sample_id != "files":
        SAMPLES.append(sample_id)
        try:
            group_dict[group_name].append(sample_id)    
        except KeyError: 
            group_dict[group_name] = [sample_id]

CUFFDIFF_DIR = "CUFFDIFF/" + "__vs__".join( sorted(group_dict)) + "/"
CUFFDIFF_FILE = CUFFDIFF_DIR + "gene_exp.diff"

ALIGN_DIR   = "ALIGN/"
STAR_DIR    = ALIGN_DIR + "STAR/"
STAR_BAM    = STAR_DIR + "{sample}.sorted.bam"
STAR_COUNTS = STAR_DIR + "{sample}.counts.tab"
STAR_LOG    = STAR_DIR + "{sample}.star.log"
STAR_SPJUNC = STAR_DIR + "{sample}.splice_junctions.bed"
STAR_FINAL_LOG = STAR_DIR + "{sample}.Log.final.out"

strand_command=""
rRNA_strand_command=""
if(config["stranded"]):
    strand_command=" --outFilterIntronMotifs RemoveNoncanonical "
    rRNA_strand_command=" --outFilterIntronMotifs RemoveNoncanonical "
else:
    rRNA_strand_command=" --outSAMstrandField intronMotif "

_mates = ['mate1', 'mate2'] if not IS_SINGLE else ['mate1']
_keepPairs = "KeepPairs" if len(_mates) == 2 else ""

rule all: 
    input: 
        expand(STAR_BAM, sample=SAMPLES),
        #ALIGNMENT_FIG,
        #GLMQLF


if IS_SINGLE:
     
    rule fastqdump__unzip_fastq_from_sra:
        input: SRAS
        output: temp(SINGLE_FASTQ),
        params: 
            sra_dir = SRA_DIR,
            sra_file = SRA_FILE_NAME,
            fastq_dir = FASTQ_DIR,
            gzip = DUMP_GZIP
        shell:
            """ cd {params.sra_dir} ; touch {input}; """
            """ fastq-dump {params.gzip} --split-3 --O {params.fastq_dir} {params.sra_file} ; """
            """ touch -c {output} ; """
    
    rule single_trim_fastqs:
        input: SINGLE_FASTQ
        output:
            trimmed_fastq = temp(SINGLE_TRIMMED_FASTQ),
            qc_html       = QC_HTML,
            qc_json       = QC_JSON
        priority: 2
        threads: 8
        shell:
            """ fastp --thread {threads} --trim_front1 5 --cut_by_quality5 --cut_by_quality3 --overrepresentation_analysis """
            """ --html {output.qc_html} --json {output.qc_json}  --in1 {input} --out1 {output.trimmed_fastq} """
     
    rule run_STAR:
        input: SINGLE_TRIMMED_FASTQ
        output:
            bam       = protected(STAR_BAM),
            counts    = STAR_COUNTS,
            log_file  = STAR_LOG,
            sjtab     = STAR_SPJUNC,
            final_log = STAR_FINAL_LOG
        params:
            min_read_len = MIN_READ_LEN,
            stranded=strand_command,
            gz_support=gz_command,
            prefix=lambda wildcards: STAR_DIR + "{sample}".format(sample=wildcards.sample),
            readgroup=lambda wildcards: "ID:{sample} PL:illumina LB:{sample} SM:{sample}".format(sample=wildcards.sample),
            keepPairs = _keepPairs
        threads: 8
        priority: 10
        shell:
            " star --runMode alignReads --runThreadN 8 "
            " --outFilterScoreMinOverLread {params.min_read_len} --outFilterMatchNminOverLread {params.min_read_len} "
            " --genomeDir {config[star_index]} "
            " --readFilesIn {input} {params.gz_support} " 
            " --outFileNamePrefix {params.prefix}. "
            " --outSAMstrandField intronMotif"
            " --outSAMmode Full --outSAMattributes All {params.stranded} "
            " --outSAMattrRGline {params.readgroup} "
            " --outSAMtype BAM SortedByCoordinate "
            " --limitBAMsortRAM 45000000000 "
            " --quantMode GeneCounts "
            " --outReadsUnmapped Fastx "
            " --outSAMunmapped Within {params.keepPairs} "
            " && touch {output.final_log} "
            " && mv {params.prefix}.Aligned.sortedByCoord.out.bam {output.bam} "
            " && mv {params.prefix}.ReadsPerGene.out.tab {output.counts} "
            " && mv {params.prefix}.SJ.out.tab {output.sjtab} "
            " && mv {params.prefix}.Log.out {output.log_file} "

else:
    
    rule fastqdump__unzip_fastq_from_sra:
        input: SRAS
        output:
            fastq_1 = temp(PAIRED_FASTQ_1),
            fastq_2 = temp(PAIRED_FASTQ_2)
        params:
            sra_dir = SRA_DIR,
            sra_file = SRA_FILE_NAME,
            fastq_dir = FASTQ_DIR,
            gzip = DUMP_GZIP
        shell:
            """ cd {params.sra_dir} ; touch {input}; """
            """ fastq-dump  {params.gzip} --split-3 --O {params.fastq_dir} {params.sra_file} ; """ 
            """ touch -c {output.fastq_1} {output.fastq_2} ; """
    
    rule fastp__trim_fastqs:
        input:
            fastq_1 = PAIRED_FASTQ_1,
            fastq_2 = PAIRED_FASTQ_2
        output:
            trimmed_pd_fq_1 = temp(PAIRED_TRIMMED_FASTQ_1),
            trimmed_pd_fq_2 = temp(PAIRED_TRIMMED_FASTQ_2),
            qc_html = QC_HTML,
            qc_json = QC_JSON,
        priority: 2
        threads: 8
        shell:
            """fastp """
            """ --cut_by_quality5 """ # enable cutting by quality in front 5' (WARNING: will interfere deduplication for PE/SE)
            """ --cut_by_quality3 """ # enable cutting by quality in tail 3' (WARNING: will interfere deduplication for SE)
            """ --detect_adapter_for_pe """  # auto-detection for adapter 
            """ --overrepresentation_analysis """ # enable overrepresented sequence analysis.
            """ --correction """                  # enable base correction in overlapped regions (only for PE data)
            """ --trim_front1 4 --trim_front2 4 """
            """ --in1  {input.fastq_1} --in2 {input.fastq_2} """
            """ --out1 {output.trimmed_pd_fq_1} --out2 {output.trimmed_pd_fq_2} """
            """ --json {output.qc_json} --html {output.qc_html} """

    rule run_STAR:
        input:
            fastq_1 = PAIRED_TRIMMED_FASTQ_1,
            fastq_2 = PAIRED_TRIMMED_FASTQ_2
        output:
            bam      = protected(STAR_BAM),
            counts   = STAR_COUNTS,
            log_file = STAR_LOG,
            sjtab    = STAR_SPJUNC,
            final_log = STAR_FINAL_LOG
        params:
            min_read_len = MIN_READ_LEN,
            stranded=strand_command,
            gz_support=gz_command,
            prefix=lambda wildcards: STAR_DIR + "{sample}".format(sample=wildcards.sample),
            readgroup=lambda wildcards: "ID:{sample} PL:illumina LB:{sample} SM:{sample}".format(sample=wildcards.sample),
            keepPairs = _keepPairs
        threads: 8
        priority: 10
        shell:
            " star --runMode alignReads --runThreadN 8 "
            " --genomeDir {config[star_index]} "
            " --readFilesIn {input.fastq_1} {input.fastq_2} "
            " {params.gz_support} "
            " --outFileNamePrefix {params.prefix}. "
            " --outSAMstrandField intronMotif"
            " --outSAMmode Full --outSAMattributes All {params.stranded} "
            " --outSAMattrRGline {params.readgroup} "
            " --outSAMtype BAM SortedByCoordinate "
            " --outFilterScoreMinOverLread {params.min_read_len} --outFilterMatchNminOverLread {params.min_read_len}  "
            " --limitBAMsortRAM 45000000000 "
            " --quantMode GeneCounts "
            " --outReadsUnmapped Fastx "
            " --outSAMunmapped Within {params.keepPairs} "
            " && touch {output.final_log} "
            " && mv {params.prefix}.Aligned.sortedByCoord.out.bam {output.bam} "
            " && mv {params.prefix}.ReadsPerGene.out.tab {output.counts} "
            " && mv {params.prefix}.SJ.out.tab {output.sjtab} "
            " && mv {params.prefix}.Log.out    {output.log_file} " 

# END IF

rule combine_alignment_logs:
    input: expand(STAR_FINAL_LOG, sample=SAMPLES)
    params: metadata = config["samples"]
    output: 
        joined = STAR_JOINED,
        formatted = STAR_JOINED_FORMATTED
    shell: 
        """ python3 /home/tyler/riken_src/programs/generate_star_mapping_table.py {input}               > {output.joined} """
        """ && /home/tyler/.local/bin//vectools join {params.metadata} {output.joined} | /home/tyler/.local/bin//vectools slice --keep-cols 0,1,-3,-2,-1 - > {output.formatted} """

rule generate_alignment_overview_figure:
    input: STAR_JOINED_FORMATTED
    params: output_type = FIG_FORMAT
    output: ALIGNMENT_FIG
    shell:
        """ /home/tyler/bin/R-3.5.1/bin/Rscript """
        """ /home/tyler/riken_src/programs/generate_alignment_overview.R {input} {output} {params.output_type}  """

rule generate_matrix: 
    input: expand(STAR_COUNTS, sample=SAMPLES)
    output: GENE_COUNT_MATRIX 
    shell: 
        """ python3 /home/tyler/riken_src/programs/samplestomatrix/samplestomatrix """
        """ --samples-as-columns --value 3 {input}  """ 
        """ | grep -v "N_"  """
        """ | sed 's/ALIGN\/STAR\///g' """
        """ | sed 's/.counts.tab//g' """
        """ > {output} """

rule edgeR__find_diff_genes:
    input:
        count_matrix = GENE_COUNT_MATRIX,
        sample_data = config["samples"]
    output:
        size_factor  = SIZE_FACTOR,
        bcv_out_file = BCV_FILE,
        glmqlf_test  = GLMQLF,
        cpm          = CPM_VALS,
        DEG_counts   = DEG_COUNTS,
        ma_plot      = MA_PLOT,
        mds          = MDS_PLOT
    shell:
        """ /home/tyler/bin/R-3.5.1/bin/Rscript """
        """ /home/tyler/riken_src/programs/edgeR/edgeR_chung.R  """
        """ {input.count_matrix} {input.sample_data} """
        """ {output.size_factor} {output.bcv_out_file} {output.glmqlf_test} """
        """ {output.cpm} {output.DEG_counts} {output.ma_plot} {output.mds} """

'''
rule edgeR__find_diff_genes:
    input: 
        count_matrix = GENE_COUNT_MATRIX,
        sample_data  = config["samples"]
    output: 
        diff_exp_genes = DIFF_EXP_GENES,
        ma_plot = MA_PLOT
    shell: 
        """ /home/tyler/bin/R-3.5.1/bin/Rscript /home/tyler/riken_src/programs/edgeR/edgeR.R {input.count_matrix} {input.sample_data} {output.diff_exp_genes} {output.ma_plot} """ 

RESULTS_DIR = "RESULTS/"
LIMMA_OUT = ""
DESEQ_OUT = ""

rule limma_and_deseq:
    input:
        counts = _getSTARcounts(config)[0]
    output:
        limma = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.limma.csv",
        deseq = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.csv",
        deseqSum = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.sum.csv",
        #annotations
        limma_annot = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.limma.annot.csv",
        deseq_annot = "analysis/" + config["token"] + "/diffexp/{comparison}/{comparison}.deseq.annot.csv",
    params:
        s1 = lambda wildcards: ",".join(config['comps'][wildcards.comparison]['control']),
        s2 = lambda wildcards: ",".join(config['comps'][wildcards.comparison]['treat']),
        gene_annotation = config['gene_annotation']
    message: "Running differential expression analysis using limma and deseq for {wildcards.comparison}"
    benchmark:
        "benchmarks/" + config["token"] + "/{comparison}.limma_and_deseq.txt"
    shell:
        "Rscript viper/modules/scripts/DEseq.R \"{input.counts}\" \"{params.s1}\" \"{params.s2}\" " 
        "{output.limma} {output.deseq} {output.limma_annot} {output.deseq_annot} "
        "{output.deseqSum} {params.gene_annotation} "
        "&& touch {output.limma} "
        "&& touch {output.limma_annot}"

rule batch_effect_removal_star:
    input:
        starmat = "analysis/" + config["token"] + "/STAR/STAR_Gene_Counts.csv",
        annotFile = config["metasheet"]
    output:
        starcsvoutput="analysis/" + config["token"] + "/STAR/batch_corrected_STAR_Gene_Counts.csv",
        starpdfoutput="analysis/" + config["token"] + "/STAR/star_combat_qc.pdf"
    params:
        batch_column="batch",
        datatype = "star"
    message: "Removing batch effect from STAR Gene Count matrix, if errors, check metasheet for batches, refer to README for specifics"
    #priority: 2
    benchmark:
        "benchmarks/" + config["token"] + "/batch_effect_removal_star.txt"
    shell:
        "Rscript viper/modules/scripts/batch_effect_removal.R {input.starmat} {input.annotFile} "
        "{params.batch_column} {params.datatype} {output.starcsvoutput} {output.starpdfoutput} "
        " && mv {input.starmat} analysis/{config[token]}/STAR/without_batch_correction_STAR_Gene_Counts.csv"
'''






