"""

"""

def check_file(file_path, file_not_found_message='File "%s" not found.'):
    assert os.path.isfile(file_path), file_not_found_message % file_path
    return file_path

# =====================================
# Config data 
# ====================================

SAMPLES_FILE          = config["samples"]
CAGE_PEAK_COORDINATES = config["cage_peak_coordinates"]

# =====================================
# IO paths
# ====================================

UNIQUE_CTSS_COUNTS    = "UNIQUE_CTSS_COUNTS/"
UNIQUE_CTSS_COUNT_BED = UNIQUE_CTSS_COUNTS + "{sample}.unique_counts.ctss.bed"

CTSS_PEAK_COUNTS              = "CTSS_PEAK_COUNTS/"
CTSS_PEAK_COUNT_INTERSECT_BED = CTSS_PEAK_COUNTS + "{sample}.peak_counts_intersect.ctss.bed"
MAPPED_PEAK_COUNTS            = CTSS_PEAK_COUNTS + "{sample}.peak_counts_mapped.ctss.bed" 
UNMAPPED_PEAK_COUNTS          = CTSS_PEAK_COUNTS + "{sample}.peak_counts_unmapped.ctss.bed"
MAPPED_PEAK_COUNTS_MERGED     = CTSS_PEAK_COUNTS + "{sample}.peak_counts_merged.ctss.bed"

COUNT_MATRICES          = "COUNT_MATRICES/"
CTSS_PEAK_COUNT_MATRIX  = COUNT_MATRICES + "rows_ctss_peak_counts_vs_cols_samples.matrix.tsv"

# ====================================
# Set goals 
# ====================================

SAMPLES = [line.strip().split("\t")[0] for line in open(SAMPLES_FILE)]

rule all:
    input:
        expand(CTSS_PEAK_COUNT_MATRIX)

# ====================================
# Rules
# ====================================

rule maps_reads_to_peaks:
    input: UNIQUE_CTSS_COUNT_BED
    params: cage_peaks = CAGE_PEAK_COORDINATES
    output: CTSS_PEAK_COUNT_INTERSECT_BED
    shell: """ 
    bedtools intersect -wao -b {params.cage_peaks} -a {input} > {output}  
    """

rule filter_reads_not_mapping_to_peaks:
    input: CTSS_PEAK_COUNT_INTERSECT_BED
    output: 
        unmapped = UNMAPPED_PEAK_COUNTS, 
        mapped   = MAPPED_PEAK_COUNTS
    shell: """
    awk '$7 == "." {{print $1"\\t"$2"\\t"$3"\\t"$4"\\t"$5"\\t"$6}}'  {input} | sort -k1,1 -k2,2n -k3,3n | gzip -c > {output.unmapped}
    awk '$7 != "." {{print $7"\\t"$8"\\t"$9"\\t"$10"\\t"$5"\\t"$6}}' {input} | sort -k1,1 -k2,2n -k3,3n | gzip -c > {output.mapped}
    """

rule merge_peak_counts:
    input: MAPPED_PEAK_COUNTS 
    output: MAPPED_PEAK_COUNTS_MERGED 
    shell: """ bedtools merge -s -d -1 -i {input} -c 4,5 -o distinct,sum | awk '{{print $1"\\t"$2"\\t"$3"\\t"$5"\\t"$6"\\t"$4}}' > {output} """

rule generate_peak_count_matrix: 
    input: expand(MAPPED_PEAK_COUNTS_MERGED, sample=SAMPLES)
    output: CTSS_PEAK_COUNT_MATRIX 
    shell: """src/programs/samplestomatrix/samplestomatrix --samples-as-columns --label 3 --value 4 {input} > {output} """


