
# Use this command to load enviormental variables on all rules.
shell.prefix("source ~/.bash_profile ;")

SRA_DIR = "SRA/"
SRAS = SRA_DIR + "{sample}.sra" 

FASTQS = "FASTQS/"
TP_FASTQS = "PAIRED_FASTQS/"
TS_FASTQS = "SINGLE_FASTQS/"

SINGLE_FASTQ   = FASTQS + "{sample}.fastq.gz"
PAIRED_FASTQ_1 = FASTQS + "{sample}_1.fastq.gz"
PAIRED_FASTQ_2 = FASTQS + "{sample}_2.fastq.gz"

PAIRED_TRIMMED_FASTQ_1 = TP_FASTQS + "{sample}_1.trimmed.fastq.gz"
PAIRED_TRIMMED_FASTQ_2 = TP_FASTQS + "{sample}_2.trimmed.fastq.gz"
SINGLE_END_TRIMMED_FASTQ = TS_FASTQS + "{sample}.trimmed.fastq.gz"

QC_DIR = "QC/"
QC_FILE = QC_DIR + "{sample}.html"

SAMPLES = [] 
for line in open(config["samples"]):
    SAMPLES.append(line.strip().split()[0])

rule all:
    input: 
        expand(QC_FILE, sample=SAMPLES)    

rule fastqdump:
    input: SRAS
    output:
       #fastq_s = temp(SINGLE_FASTQ),
       fastq_1 = temp(PAIRED_FASTQ_1),
       fastq_2 = temp(PAIRED_FASTQ_2)
    params: fastq_dir = FASTQS
    shell: """
    fastq-dump --gzip --split-3 --O {params.fastq_dir} {input}  #{wildcards.sample} ;
    touch -c {output.fastq_1} {output.fastq_2} ; 
    """

#ruleorder: trim_single_fastqs > trim_paired_fastqs 
#rule trim_single_fastqs:
#    input: SINGLE_FASTQ
#    output: 
#        fastq = SINGLE_END_TRIMMED_FASTQ,
#        html = QC_FILE
#    shell:
#        """ fastp --trim_front1 5 --cut_by_quality5 --cut_by_quality3 --overrepresentation_analysis --html {output.html} --in1 {input} --out1 {output.fastq} """

rule trim_paired_fastqs:
    input:
        fastq_1 = PAIRED_FASTQ_1,
        fastq_2 = PAIRED_FASTQ_2
    output: 
        trimmed_fastq_1 = PAIRED_TRIMMED_FASTQ_1,
        trimmed_fastq_2 = PAIRED_TRIMMED_FASTQ_2,
        html = QC_FILE
    shell: 
        """fastp """
        """ --cut_by_quality5 """             # enable per read cutting by quality in front (5'), default is disabled (WARNING: this will interfere deduplication for both PE/SE data)  
        """ --cut_by_quality3 """             # enable per read cutting by quality in tail (3'), default is disabled (WARNING: this will interfere deduplication for SE data)
        """ --detect_adapter_for_pe """       # by default, the auto-detection for adapter is for SE data input only, turn on this option to enable it for PE data. 
        """ --overrepresentation_analysis """ # enable overrepresented sequence analysis.
        """ --correction """                  # enable base correction in overlapped regions (only for PE data), default is disabled
        """ --trim_front1 5 --trim_front2 5 """
        """ --in1  {input.fastq_1} --in2 {input.fastq_2} --html {output.html} --out1 {output.trimmed_fastq_1} --out2 {output.trimmed_fastq_2} """




