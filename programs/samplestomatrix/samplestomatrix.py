#!/usr/bin/env python3
"""
A simple program to covert delimited text files, which represent individual samples, into a matrix.
"""
import argparse


def create_dict_from_files(sample_list, label_column, value_column, input_delimiter):

    assert type(label_column) is int
    assert type(value_column) is int

    sample_names = dict()
    all_gene_ids = set()

    for file_name in sample_list:

        # @TODO: Allow users to upload sample ID extraction regex.
        sample_name = file_name  # .split("/")[-1].split(".")[0]
        sample_names[sample_name] = dict()

        with open(file_name) as file_obj:

            for line in file_obj:

                if line[0] != "#":

                    sl = line.strip().split(input_delimiter)
                    tmp_gene_id = sl[label_column]
                    tmp_exp_val = sl[value_column]
                    sample_names[sample_name][tmp_gene_id] = tmp_exp_val
                    all_gene_ids.add(tmp_gene_id)

    return sample_names, all_gene_ids


def convert_dict_to_matrix(sample_names, all_gene_ids, min_val, out_delimiter, not_found_val="0", samples_as_rows=True):
    """

    :param sample_names:
    :param all_gene_ids:
    :param out_delimiter:
    :param not_found_val:
    :return:
    """
    if samples_as_rows:
        column_keys = sorted(all_gene_ids)
        row_keys = sorted(sample_names)
    else:
        column_keys = sorted(sample_names)
        row_keys = sorted(all_gene_ids)

    title = ["ROW_ID"] + sorted(column_keys)
    print(out_delimiter.join(title))
    for row_name in row_keys:
        tmp_out_list = [row_name]
        for column_name in column_keys:
            try:
                if samples_as_rows:
                    out_val = sample_names[row_name][column_name] 
                else:
                    out_val = sample_names[column_name][row_name]
            except KeyError:
                out_val = not_found_val
            tmp_out_list.append(out_val)

        if max([int(i) for i in tmp_out_list[1:]]) > min_val:
            print(out_delimiter.join(tmp_out_list))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--delimiter', type=str, default="\t", help='The delimiter that separates values.')
    parser.add_argument('--label', type=int, default=0, help='Column that represents labels (zero-based counting).')
    parser.add_argument('--value', type=int, default=-1, help='Column that represents values (zero-based counting).')

    parser.add_argument('--min-val', type=int, default=20, help='A simple filter to remove untestable genes.')

    parser.add_argument('--samples-as-columns', action='store_false', help='')

    parser.add_argument('samples', nargs='+', help='A list of files')

    args = parser.parse_args()

    delimiter = args.delimiter

    sample_names_list, all_unique_gene_ids = create_dict_from_files(args.samples, args.label, args.value,  delimiter)

    convert_dict_to_matrix(sample_names_list, all_unique_gene_ids, args.min_val, delimiter,
                           not_found_val="0", samples_as_rows=args.samples_as_columns)

