"""
python3 -m unittest unittests.py
"""
import unittest
from samplestomatrix import create_dict_from_files, convert_dict_to_matrix
import sys
from contextlib import contextmanager
from io import StringIO


def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, same

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


class TestStringMethods(unittest.TestCase):

    def test_parser(self):

        # f1_text = """A\t1\nB\t2\nC\t3\n"""
        # f2_text = """A\t12\nB\t23\nC\t34\n"""

        expected_all_gene_ids = {"geneA", "geneB"}
        expected_sample_names = {
            "sample1": {"geneA": "1",  "geneB": "3"},
            "sample2": {"geneA": "50", "geneB": "10"}
        }
        sample_list = ["test_data/sample1.csv", "test_data/sample2.csv"]
        sample_names, all_gene_ids = create_dict_from_files(sample_list, 0, 1, ",")

        print(expected_sample_names)

        print(sample_names)
        print("***")
        self.assertTrue(dict_compare(sample_names, expected_sample_names))
        self.assertEqual(all_gene_ids, expected_all_gene_ids)

    @contextmanager
    def test_print_matrix(self):
        expected_all_gene_ids = {"geneA", "geneB"}
        expected_sample_names = {
            "sample1": {"geneA": "1",  "geneB": "3"},
            "sample2": {"geneA": "50", "geneB": "10"}
        }
        expected_output = """ROW_ID,geneA,geneB\nsample1,1,3\nsample2,50,10"""

        with captured_output() as (out, err):
            convert_dict_to_matrix(expected_sample_names, expected_all_gene_ids, ",")
        # This can go inside or outside the `with` block
        output = out.getvalue().strip()
        self.assertEqual(output, expected_output)

    def test_print_inverted_matrix(self):
        expected_all_gene_ids = {"geneA", "geneB"}
        expected_sample_names = {
            "sample1": {"geneA": "1",  "geneB": "3"},
            "sample2": {"geneA": "50", "geneB": "10"}
        }
        # expected_output = """ROW_ID,geneA,geneB\nsample1,1,3\nsample2,50,10"""
        expected_output = """ROW_ID,sample1,sample2\ngeneA,1,50\ngeneB,3,10"""

        with captured_output() as (out, err):
            convert_dict_to_matrix(expected_sample_names, expected_all_gene_ids, ",", "0", False)
        # This can go inside or outside the `with` block
        output = out.getvalue().strip()
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

