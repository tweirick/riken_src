import sys

unique_mapped = "Uniquely mapped reads number |\t"
multi_mapped = "Number of reads mapped to multiple loci |\t"

unmapped_mismatch = "% of reads unmapped: too many mismatches |\t"
unmapped_short    = "% of reads unmapped: too short |\t"
unmapped_other    = "% of reads unmapped: other |\t"

def get_value(parse_str, in_line):
    p = in_line.split("|")[-1].strip().strip("%")
    return round(float(p), 2)

# sid   Condition   Unique_Mapped_Reads Multi_Mapped_Reads  Percent_Unmapped
print("\t".join(["files", "Unique_Mapped_Reads", "Multi_Mapped_Reads", "Percent_Unmapped"]))

for file_name in sys.argv[1:]:
    unique_mapped_int       = 0
    multi_mapped_int        = 0
    unmapped_mismatch_float = 0
    unmapped_short_float    = 0
    unmapped_other_float    = 0
    sample_id = file_name.split(".")[0].split("/")[-1]
    with open(file_name) as f_obj:
        for line in f_obj:
            if unique_mapped in line:
                unique_mapped_int = get_value(unique_mapped, line)
            elif multi_mapped      in line:
                multi_mapped_int = get_value(multi_mapped, line)
            elif unmapped_mismatch in line:
               unmapped_mismatch_float = get_value(unmapped_mismatch, line)
            elif unmapped_short    in line:
               unmapped_short_float = get_value(unmapped_short_float, line)
            elif unmapped_other    in line:
               unmapped_other_float = get_value(unmapped_other_float, line)
            
    print("\t".join([
         sample_id, 
         str(unique_mapped_int), 
         str(multi_mapped_int), 
         str(round(unmapped_mismatch_float + unmapped_short_float + unmapped_other_float, 2))
       ])
    )


