import sys

fdr_cutoff = 0.05
logfc_cutoff = 0.2

total_fdr_up   = 0
total_fdr_down = 0

total_fdr_and_min_log_fc_up = 0
total_fdr_and_min_log_fc_down = 0

total = 0

with open(sys.argv[1]) as f_obj:
    titles = f_obj.readline()
    for line in f_obj:
        sl = line.strip().split()
        log_fc = float(sl[1])
        fdr = float(sl[-1])
         
        if log_fc > 0:
            if fdr < fdr_cutoff:
                total_fdr_up += 1
                if log_fc > logfc_cutoff:
                    total_fdr_and_min_log_fc_up += 1
            else: 
                 total += 1
        else: 
           if fdr < fdr_cutoff:
               total_fdr_down += 1
               if abs(log_fc) > logfc_cutoff:
                   total_fdr_and_min_log_fc_down += 1 
           else: 
              total += 1

print("Non-sig %s" % total)
print("FDR less than %s" % fdr_cutoff)
print("Up %s"   % total_fdr_up  ) 
print("Down %s" % total_fdr_down )
print("Passing FDR and fold change over %s" % logfc_cutoff)
print("Up %s"   % total_fdr_and_min_log_fc_up   )
print("Down %s" % total_fdr_and_min_log_fc_down )



