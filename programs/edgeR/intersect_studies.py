import sys 

fdr_cutoff   = float(sys.argv[1])
logfc_cutoff = float(sys.argv[2])

fold_changes = {}
shared_genes = {}
gene_dict = {}
for file_name in sys.argv[3:]:
    with open(file_name) as f_obj:
        title = f_obj.readline()
        for line in f_obj:
            sl = line.strip().split()

            gene_id = sl[0].split(".")[0]
            log_fc = float(sl[1])
            fdr = float(sl[-1])

            try:
                shared_genes[gene_id] += 1
            except KeyError:
                shared_genes[gene_id] = 1

            if abs(log_fc) > logfc_cutoff and fdr < fdr_cutoff:
        
                try:
                    fold_changes[gene_id].append(log_fc)
                    gene_dict[gene_id].append(file_name)
                except KeyError: 
                    fold_changes[gene_id] = [log_fc]
                    gene_dict[gene_id] = [file_name]

# Count shared IDs
share_counts = {}
for name in shared_genes:
    cnt = shared_genes[name]
    try:
        share_counts[cnt] += 1
    except KeyError:
        share_counts[cnt] = 1
     
print("#Count_Number Counts")
for count in sorted(share_counts):
    print(count, share_counts[count])

combis = {}
combis_agree = {}
for gene_name in gene_dict:
    groups = ",".join( sorted(gene_dict[gene_name]) ) 
    t = fold_changes[gene_name]
    matching = all(i < 0 for i in t) or all(i > 0 for i in t)

    try:
        combis[groups] += 1
    except KeyError:
        combis[groups] = 1

    if matching:
        try: 
            combis_agree[groups] += 1
        except KeyError: 
            combis_agree[groups] = 1

print("#group counts")
for c in combis:
    print(c, combis[c])
    print(c, combis_agree[c])
    
