"""

"""


def parse_psl_file(psl_file):
    """

    :param psl_file:
    :return:
    """
    blat_dict = {}
    for line in open(psl_file, "r"):  # summarize the blat hits
        if len(line) > 0 and line[0] != "#":
            psl_fields = line.split()

            chromosome, pos, ref, alt, mm_read_count = psl_fields[9].split("-")
            var_tuple = (chromosome, int(pos), ref, alt)

            # number of Matches, targetName, block_count, blockSize, targetStarts
            blat_score = [psl_fields[0], psl_fields[13], psl_fields[17], psl_fields[18], psl_fields[20]]

            if var_tuple not in blat_dict:
                blat_dict[var_tuple] = []
            blat_dict[var_tuple].append(blat_score)

    return blat_dict


def generate_keep_dict(blat_dict):
    site_dict = {}
    discard_dict = {}
    # Loop over blat Hits
    for var_tuple in blat_dict.keys():  # Loop over all blat hits of mmReads to observe the number of alignments
        keep_snp = False
        chromosome, pos, ref, alt = var_tuple
        psl_line = blat_dict[var_tuple]
        largest_score = 0
        largest_score_line = psl_line[0]
        score_array = []

        # Look for largest blatScore and save the largest line too
        for blat_hit in psl_line:
            line_score = int(blat_hit[0])
            score_array.append(line_score)
            if line_score > largest_score:
                largest_score = line_score
                largest_score_line = blat_hit

        score_array.sort(reverse=True)
        # Test if more than one blat hit exists
        if len(score_array) < 2:
            score_array.append(0)

        # check if same chromosome and hit is lower than 95 percent of first hit
        if chromosome == largest_score_line[1] and score_array[1] < score_array[0] * 0.95:

            block_count = int(largest_score_line[2])
            block_sizes = largest_score_line[3].split(",")[:-1]
            block_starts = largest_score_line[4].split(",")[:-1]

            for i in range(block_count):
                # @TODO: Ask David about this coordinate change. It seems like one should subtract one from here.
                start_pos = int(block_starts[i]) + 1
                end_pos = start_pos + int(block_sizes[i])
                if start_pos <= pos < end_pos:  # Check if alignment overlaps mismatch.
                    keep_snp = True

        if keep_snp:
            if var_tuple in site_dict:
                site_dict[var_tuple] += 1
            else:
                site_dict[var_tuple] = 1
        else:  # when read does not pass the blat criteria
            if var_tuple in discard_dict:
                discard_dict[var_tuple] += 1
            else:
                discard_dict[var_tuple] = 1

    return site_dict, discard_dict


def filter_vcf_with_psl(psl_file, vcf_file):

    #  (chromosome, position, ref, alt): [number of Matches, targetName, block_count, blockSize, targetStarts]
    blat_dict = parse_psl_file(psl_file)

    site_dict, discard_dict = generate_keep_dict(blat_dict)

    for line in open(vcf_file):
        if line[0] == "#":
            print(line.strip())
        else:
            chromosome, pos, id, ref, alt = line.split()[:5]
            if (chromosome, int(pos), ref, alt) in site_dict:
                print(line.strip())

    # print(len(site_dict), len(discard_dict))
    # for vcf_tuple in
    # =================================================
    """
    mmNumberTotal = 0
    mmNumberTooSmall = 0
    mmReadsSmallerDiscardReads = 0
    for key in variants.variantDict.keys():
        numberBlatReads = 0
        numberDiscardReads = 0
        if key in site_dict:
            numberBlatReads = site_dict[key]
        if key in discard_dict:
            numberDiscardReads = discardDict[key]
        if numberBlatReads <= min_mismatch and numberBlatReads <= numberDiscardReads:
            del variants.variantDict[key]
        # count statistics
        if numberBlatReads < min_mismatch:
            mmNumberTooSmall += 1
        elif numberBlatReads < numberDiscardReads:  # check if more reads fit the blat criteria than not
            mmReadsSmallerDiscardReads += 1
        mmNumberTotal += 1
        # output statistics
        mmPassedNumber = mmNumberTotal - (mmNumberTooSmall + mmReadsSmallerDiscardReads)
    """

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--vcf',
                        type=str,
                        required=True,
                        help="A vcf file.")

    parser.add_argument('--psl',
                        type=str,
                        required=True,
                        help="A psl file.")

    args = parser.parse_args()

    filter_vcf_with_psl(args.psl, args.vcf)
