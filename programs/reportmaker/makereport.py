#!/usr/bin/env python3

import argparse
from string import whitespace
import base64

# border-style: solid;
title_template = '<div style="border-style: solid; border-width: 2px 0px 0px 0px"></div>' \
                 '<h3 style="width: 50%%; border-style: solid; border-width: 0px 0px 1px 0px;">%s</h3>'

text_template = "<div>%s</div>"

table_suffix_template1 = """
<script data-config>
    var filtersConfig = {
        base_path: 'tablefilter/',
        paging: {
          results_per_page: ['Records: ', [10, 25, 50, 100]]
        },
        state: {
          types: ['local_storage'],
          filters: true,
          page_number: true,
          page_length: true,
          sort: true
        },
        alternate_rows: true,
        btn_reset: true,
        rows_counter: true,
        loader: {
          html: '<div id="lblMsg"></div>',
          css_class: 'myLoader'
        },
        status_bar: {
          target_id: 'lblMsg',
          css_class: 'myStatus'
        },
        
        // Must be 'string' or 'number'
        col_types: [ """

table_suffix_template2 = """ ],
        extensions:[{
            name: 'sort'
        }]
    };
    var tf = new TableFilter('demo', filtersConfig);
    tf.init();
</script>

<style type="text/css" data-config>
    /**
     * Custom CSS definitions as per TableFilter configuration
     */
    .myLoader{
        position:absolute; padding: 5px;
        margin:100px 0 0 5%; width:auto;
        z-index:1000; font-size:12px; font-weight:bold;
        border:1px solid #666; background:#ffffcc;
        vertical-align:middle;
    }
    .myStatus{
        width:auto; display:block;
    }
</style>

<script>
  (function(loadedPolyfills, document) {
    if(loadedPolyfills.indexOf('window.Promise') !== -1) {
      var msg = '<br><div class="alert alert-danger" role="alert">'
        + 'Your browser does not support <code>Promise</code>.<br>'
        + 'Use a polyfill to provide support for the missing language feature, eg:'
        + '<pre>https://unpkg.com/es6-promise@3.2.1/dist/es6-promise.min.js</pre>'
        + '</div>';
        document.write(msg);
    }
    if(loadedPolyfills.indexOf('Function.name') !== -1) {
      var msg = '<br><div class="alert alert-danger" role="alert">'
        + 'Your browser does not support <code>Function.name</code>.<br>'
        + 'Use a polyfill to provide support for the missing language feature, eg:'
        + '<pre>https://unpkg.com/function.name-polyfill@latest/Function.name.js</pre>'
        + '</div>';
        document.write(msg);
    }
  })(loadedPolyfills, document)
</script>

"""


def add_image(image_file_name, title=None, text=None):
    table_list = []

    if title:
        table_list.append(title_template % title)

    if text:
        table_list.append(text_template % text)

    data_uri = base64.b64encode(open(image_file_name, 'rb').read()).decode('utf-8').replace('\n', '')
    table_list.append('<img style="max-width:80%%;" src="data:image/png;base64,%s">' % data_uri)

    return "\n".join(table_list)


def add_table(table_file_name, title=None, text=None, delimiter="\t"):

    table_list = ['<table id="demo" class="countries">\n']

    if title:
        table_list.append(title_template % title)

    if text:
        table_list.append(text_template % text)

    with open(table_file_name) as f_obj:
        # First line treat as titles
        titles = f_obj.readline().strip()

        if set(titles[0]) in set(whitespace):
            title_list = ["<th>&nbsp;</th>"] + ["<th>" + cell + "</th>" for cell in titles.split(delimiter)]
        else:
            title_list = ["<th>" + cell + "</th>" for cell in titles.split(delimiter)]

        # Here setting True indicates column is numeric.
        column_data_types = [True for el in title_list]

        table_list.append("<tr>%s</tr>" % "".join(title_list))

        for line in f_obj:
            sp = line.strip().split(delimiter)
            table_list.append("<tr>%s</tr>" % "".join(["<td>%s</td>" % c for c in sp]))

            # Update columns data type info.
            for i in range(0, len(sp)):
                el = sp[i]
                try:
                    is_numeric = sp == "NA" or el == "na" or float(el)
                except ValueError:
                    is_numeric = False

                if column_data_types[i]:
                    column_data_types[i] = is_numeric

    table_list.append("</table>")
    column_data_types = ["'number'" if el else "'string'" for el in column_data_types]

    table_suffix = table_suffix_template1 + ", ".join(column_data_types) + table_suffix_template2
    return "\n".join(table_list) + "\n" + table_suffix


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--template',      type=str, default="\t", required=True, help='')
    parser.add_argument('--template-text', type=str, default="\t", help='')

    parser.add_argument('--image',         type=str, default=None, help='')
    parser.add_argument('--image-title',   type=str, default=None, help='')
    parser.add_argument('--image-text',    type=str, default=None, help='')

    parser.add_argument('--table',         type=str, default=None, help='')
    parser.add_argument('--table-title',   type=str, default=None, help='')
    parser.add_argument('--table-text',    type=str, default=None, help='')
    parser.add_argument('--delimiter',     type=str, default="\t", help='')

    args = parser.parse_args()
    new_text = ""
    assert args.image != args.table, "Only add one image or table at a time."

    template_f_obj = open(args.template).read()
    if args.image:
        new_text = add_image(args.image, title=args.image_title, text=args.image_text)
    elif args.table:
        new_text = add_table(args.table, title=args.table_title, text=args.table_text, delimiter=args.delimiter)

    print(template_f_obj + new_text)


